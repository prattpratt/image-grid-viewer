# README #

### What is this repository for? ###

This is just sample Android app that :

* Declares custom grid view component (extends *View*) w/ custom attributes
* Uses that component for showing user images sorted by capture date
* Uses *MediaStore* content provider to request images
* The app is compatible w/ Android 2.3.3+ (API 10+)

### How do I get set up? ###

* Get *Android SDK*, install latest Android Platform and some extras:
	* *Android Support Repository*
	* *Android Support Library*
* Use *IDEA (Android-Gradle)* or *Eclipse (ADT/Gradle)* or *Gradle* or *Maven* to build the project
* Before using *Eclipse (ADT)* have a look at [Support Library Setup](http://developer.android.com/tools/support-library/setup.html)
* Before using *Eclipse (Gradle)* you need to install [Gradle Integration for Eclipse](http://marketplace.eclipse.org/content/gradle-integration-eclipse-37-43) plugin
* Before using *Maven* have a look at [android-maven-plugin](https://github.com/simpligility/android-maven-plugin) and [maven-android-sdk-deployer](https://github.com/simpligility/maven-android-sdk-deployer)
* Run using default AVD from SDK or Genymotion device or real Android device

### Who do I talk to? ###

* @prattpratt