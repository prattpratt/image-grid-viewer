package com.pratt.android.gridviewer.providers;

import static android.provider.BaseColumns._ID;
import static android.provider.MediaStore.Images.ImageColumns.DATE_TAKEN;
import static android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.database.Cursor;

public class ImageRetriver {

	private static final String[] PROJECTION = new String[] { _ID, DATE_TAKEN };
	private static final String SORT_ORDER = DATE_TAKEN + " ASC";

	private static final DateFormat DATE_FORMAT = DateFormat.getDateInstance();

	/** The system content resolver. */
	private ContentResolver mContentResolver;

	/** The list of image items grouped per capture date */
	private List<ImageGridItem> mItems = new ArrayList<ImageGridItem>();

	/**
	 * Instantiates a new image retriver.
	 */
	public ImageRetriver(ContentResolver cr) {
		mContentResolver = cr;
	}

	/**
	 * Retrive all images from MediaStore
	 */
	public void retrive() {
		Cursor cursor = mContentResolver.query(
				EXTERNAL_CONTENT_URI,
				PROJECTION,
				null,
				null,
				SORT_ORDER);

		String imageDate, prevImageDate, imageURI;
		List<String> imageURIs = new ArrayList<String>();

		if (cursor.moveToFirst()) {
			prevImageDate = getFormattedImageDate(cursor);
			do {
				imageURI = getImageURI(cursor);
				imageDate = getFormattedImageDate(cursor);

				if (imageDate.equals(prevImageDate)) {
					imageURIs.add(imageURI);
				} else {
					mItems.add(new ImageGridItem(prevImageDate, imageURIs));
					imageURIs = new ArrayList<String>();
					imageURIs.add(imageURI);
				}

				prevImageDate = imageDate;
			} while (cursor.moveToNext());

			mItems.add(new ImageGridItem(prevImageDate, imageURIs));
		}
	}

	/**
	 * Gets the formatted image date from cursor on current position.
	 *
	 * @param cursor
	 *            the cursor
	 * @return the formatted image date
	 */
	private String getFormattedImageDate(Cursor cursor) {
		int dateIndex = cursor.getColumnIndexOrThrow(DATE_TAKEN);
		long millis = cursor.getLong(dateIndex);
		Date date = new Date(millis);
		return DATE_FORMAT.format(date);
	}

	/**
	 * Gets the image uri from cursor on current position.
	 *
	 * @param cursor
	 *            the cursor
	 * @return the image URI
	 */
	private String getImageURI(Cursor cursor) {
		int idIndex = cursor.getColumnIndexOrThrow(_ID);
		long id = cursor.getLong(idIndex);
		return ContentUris.withAppendedId(EXTERNAL_CONTENT_URI, id).toString();
	}

	/**
	 * Gets the retrived images from MedisStore
	 *
	 * @return the list of image items grouped per capture date
	 */
	public List<ImageGridItem> getImages() {
		return mItems;
	}
}
