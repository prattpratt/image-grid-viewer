package com.pratt.android.gridviewer.providers;

import java.util.List;

public class ImageGridItem {

	private String formattedDate;
	private List<String> imageURIs;

	public ImageGridItem() {}

	public ImageGridItem(String formattedDate, List<String> imageURIs) {
		this.formattedDate = formattedDate;
		this.imageURIs = imageURIs;
	}

	public String getDateTaken() {
		return formattedDate;
	}

	public void setDateTaken(String formattedDate) {
		this.formattedDate = formattedDate;
	}

	public List<String> getImageURIs() {
		return imageURIs;
	}

	public void setImageURIs(List<String> uris) {
		this.imageURIs = uris;
	}

	@Override
	public String toString() {
		return "{formattedDate=" + formattedDate + ", imageURIs=" + imageURIs + "}";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((formattedDate == null) ? 0 : formattedDate.hashCode());
		result = prime * result + ((imageURIs == null) ? 0 : imageURIs.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ImageGridItem other = (ImageGridItem) obj;
		if (formattedDate == null) {
			if (other.formattedDate != null)
				return false;
		} else if (!formattedDate.equals(other.formattedDate))
			return false;
		if (imageURIs == null) {
			if (other.imageURIs != null)
				return false;
		} else if (!imageURIs.equals(other.imageURIs))
			return false;
		return true;
	}


}
