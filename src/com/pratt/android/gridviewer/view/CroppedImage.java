package com.pratt.android.gridviewer.view;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.View;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.nostra13.universalimageloader.core.assist.ViewScaleType;
import com.nostra13.universalimageloader.core.imageaware.ImageAware;
import com.nostra13.universalimageloader.core.imageaware.NonViewAware;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.process.BitmapProcessor;

/**
 * The image to be loaded from source URI, processed (cropped), memory cached and ready for displaying.
 * <b>UIL</b> library is used for async image loading.
 */
final class CroppedImage implements ImageLoadingListener {

	/** The source URI address. */
	private String mSourceUri;

	/** The bitmap to be displayed. */
	private Bitmap mBitmap;

	/** The pseudo-view for image displaying */
	private ImageAware mImageAware;

	/** The container that manages loading images. */
	private ImageLoadable mContainer;

	public CroppedImage(String sourceUri, ImageLoadable container) {
		mSourceUri = sourceUri;
		mContainer = container;
	}

	/**
	 * Gets the source URI.
	 *
	 * @return the source URI
	 */
	public String getSourceUri() {
		return mSourceUri;
	}

	/**
	 * Gets the underlying bitmap.
	 *
	 * @return the bitmap or possibly <code>null</code>
	 */
	public Bitmap getBitmap() {
		return mBitmap;
	}

	/**
	 * Checks if image processing was started.
	 *
	 * @return true, if successful
	 */
	public boolean isProcessing() {
		return mImageAware != null;
	}

	/**
	 * Start image loading, processing (crop) and memory caching.
	 *
	 * @param imageSize
	 *            the target image size after crop
	 */
	private void startProcessing(ImageSize imageSize) {
		mImageAware = new NonViewAware(/* mSourceUri, */imageSize, ViewScaleType.CROP);
		DisplayImageOptions opts = buildCroppedDisplayOpts(imageSize.getWidth(), imageSize.getHeight());
		ImageLoader.getInstance().displayImage(mSourceUri, mImageAware, opts, this);
	}

	/**
	 * Cancel processing task.
	 */
	public void cancelProcessing() {
		if (isProcessing()) {
			ImageLoader.getInstance().cancelDisplayTask(mImageAware);
		}
	}

	@Override
	public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
		mBitmap = loadedImage;
		mContainer.invalidate();
	}

	@Override
	public void onLoadingStarted(String imageUri, View view) {
		mBitmap = mContainer.getLoadingImage();
		mContainer.invalidate();
	}

	@Override
	public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
		mBitmap = mContainer.getErrorImage();
		mContainer.invalidate();
	}

	@Override
	public void onLoadingCancelled(String imageUri, View view) {
		mBitmap = mContainer.getCanceledImage();
		mContainer.invalidate();
	}

	/**
	 * Draw the cropped image on provided canvas
	 *
	 * @param canvas
	 *            the canvas
	 * @param left
	 *            the position of the left side of image being drawn
	 * @param top
	 *            the position of the top side of image being drawn
	 * @param imageSize
	 *            the desired (cropped) image size
	 * @param paint
	 *            the paint used to draw the bitmap (may be null)
	 */
	public void draw(Canvas canvas, int left, int top, ImageSize imageSize, Paint paint) {
		if (!isProcessing()) {
			startProcessing(imageSize);
		}
		canvas.drawBitmap(mBitmap, left, top, paint);
	}

	/**
	 * Builds display options for crop image pre-processing.
	 *
	 * @param cropWidth
	 *            the crop width
	 * @param cropHeight
	 *            the crop height
	 * @return the display image options
	 */
	private final static DisplayImageOptions buildCroppedDisplayOpts(int cropWidth, int cropHeight) {
		return new DisplayImageOptions.Builder()
				.cacheInMemory(true)
				.cacheOnDisk(false)
				.preProcessor(new ImageCropProcessor(cropWidth, cropHeight))
				.build();
	}

	/**
	 * Class to process bitmap cropping
	 */
	private static class ImageCropProcessor implements BitmapProcessor {
		private int mCropWidth;
		private int mCropHeight;

		public ImageCropProcessor(int cropWidth, int cropHeight) {
			mCropWidth = cropWidth;
			mCropHeight = cropHeight;
		}

		@Override
		public Bitmap process(Bitmap bitmap) {
			return cropBitmap(bitmap, mCropWidth, mCropHeight);
		}

		/**
		 * Crop source bitmap. Stretch bitmap canvas if its width/height is less then target dimensions.
		 *
		 * @param src
		 *            the source bitmap
		 * @param width
		 *            the target width
		 * @param height
		 *            the target height
		 * @return the new cropped bitmap or source bitmap
		 */
		private final static Bitmap cropBitmap(Bitmap src, int width, int height) {
			final int srcWidth = src.getWidth();
			final int srcHeight = src.getHeight();

			if (srcWidth == width && srcHeight == height) {
				return src;
			}

			Bitmap bitmap = Bitmap.createBitmap(width, height, Config.ARGB_8888);
			Canvas canvas = new Canvas(bitmap);
			canvas.drawBitmap(src, 0, 0, null);
			canvas.setBitmap(null);
			src.recycle();
			return bitmap;
		}
	}
}
