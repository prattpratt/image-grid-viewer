package com.pratt.android.gridviewer.view;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.GridView;

import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.pratt.android.gridviewer.R;

/**
 * A view that shows images in two-dimensional grid with specified padding and rows/columns.
 *
 * @see GridView
 *
 * @attr ref {@link R.styleable#ImageGridView_columns}
 * @attr ref {@link R.styleable#ImageGridView_rows}
 * @attr ref {@link R.styleable#ImageGridView_padding}
 */
public class ImageGridView extends View implements ImageLoadable {

	private static final int DEFAULT_ROWS = 3;
	private static final int DEFAULT_COLUMNS = 3;
	private static final int DEFAULT_TILE_PADDING = 5;

	/** Number of tile rows. */
	private int mRows = DEFAULT_ROWS;

	/** Number of tile columns. */
	private int mColumns = DEFAULT_COLUMNS;

	/** Padding between tiles. */
	private int mTilePadding = DEFAULT_TILE_PADDING;

	/** The width or height for every image tile in grid. */
	private int mTileWidth;

	/** The every image tile size. */
	private ImageSize mImageSize;

	/** The paint object. */
	private Paint mPaint;

	/** The loadable images. */
	private List<CroppedImage> mImages;

	/** The bitmap for empty tile. */
	private Bitmap mEmptyBitmap;

	/** The bitmap for image loading error. */
	private Bitmap mErrorBitmap;

	/** The bitmap for cancelled image loading. */
	private Bitmap mCancelBitmap;

	/** The bitmap displayed while image loading. */
	private Bitmap mLoadingBitmap;

	/* =========================== CONSTRUCTORS =========================== */

	/**
	 * Instantiates a new image grid view.
	 *
	 * @param context
	 *            the context
	 */
	public ImageGridView(Context context) {
		super(context);
		init();
	}

	/**
	 * Instantiates a new image grid view.
	 *
	 * @param context
	 *            the context
	 * @param attrs
	 *            The desired attributes to be retrieved.
	 */
	public ImageGridView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	/**
	 * Instantiates a new image grid view.
	 *
	 * @param context
	 *            the context
	 * @param attrs
	 *            The desired attributes to be retrieved.
	 * @param defStyleAttr
	 *            An attribute in the current theme that contains a reference to a style resource that supplies defaults
	 *            values for the TypedArray. Can be 0 to not look for defaults.
	 */
	public ImageGridView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);

		TypedArray a = context.obtainStyledAttributes(
				attrs,
				R.styleable.ImageGridView,
				defStyleAttr,
				0);

		try {
			mRows = a.getInteger(R.styleable.ImageGridView_rows, DEFAULT_ROWS);
			mColumns = a.getInteger(R.styleable.ImageGridView_columns, DEFAULT_COLUMNS);
			mTilePadding = a.getDimensionPixelSize(R.styleable.ImageGridView_padding, DEFAULT_TILE_PADDING);
		} finally {
			a.recycle();
		}

		init();
	}

	private void init() {
		mImages = new ArrayList<CroppedImage>();
		mPaint = new Paint();
	}

	/* =========================== VIEW PROPERTY SETTERS =========================== */

	/**
	 * Set the images to grid view.
	 *
	 * @param imageURIs
	 *            the image URIs
	 */
	public void setImages(List<String> imageURIs) {
		// cancel all previous image processing tasks
		for (CroppedImage image : mImages) {
			image.cancelProcessing();
		}

		// clear images list
		mImages.clear();

		// add new images
		for (String uri : imageURIs) {
			mImages.add(new CroppedImage(uri, this));
		}
	}

	/**
	 * Set the images to the grid view.
	 *
	 * @param imageURIs
	 *            the images URIs
	 */
	public void setImages(String... imageURIs) {
		setImages(Arrays.asList(imageURIs));
	}

	/**
	 * Sets the rows number.
	 *
	 * @param rows
	 *            the new rows
	 */
	public void setRows(int rows) {
		if (mRows != rows) {
			mRows = rows;
			requestLayoutIfNecessary();
		}
	}

	/**
	 * Sets the columns number.
	 *
	 * @param columns
	 *            the new columns
	 */
	public void setColumns(int columns) {
		if (mColumns != columns) {
			mColumns = columns;
			requestLayoutIfNecessary();
		}
	}

	/**
	 * Sets the tile padding.
	 *
	 * @param padding
	 *            the new tile padding
	 */
	public void setTilePadding(int padding) {
		if (mTilePadding != padding) {
			mTilePadding = padding;
			requestLayoutIfNecessary();
		}
	}

	/* =========================== VIEW PROPERTY GETTERS =========================== */

	/**
	 * Gets the images list size.
	 *
	 * @return the images size
	 */
	public int getSize() {
		return mImages.size();
	}

	/**
	 * Gets the rows number.
	 *
	 * @return the rows
	 */
	public int getRows() {
		return mRows;
	}

	/**
	 * Gets the columns number.
	 *
	 * @return the columns
	 */
	public int getColumns() {
		return mColumns;
	}

	/**
	 * Gets the tile padding.
	 *
	 * @return the tile padding
	 */
	public int getTilePadding() {
		return mTilePadding;
	}

	/* =========================== VIEW LIFECYCLE CALLBACKS =========================== */

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);

		// Try for a width based on our minimum
		int minw = getPaddingLeft() + getPaddingRight() + getSuggestedMinimumWidth();
		int w = ViewCompat.resolveSizeAndState(minw, widthMeasureSpec, 1);

		int newWidth = (int) ((w - mTilePadding) / mColumns - mTilePadding);

		// if view settings did not changed then do not re-create empty and error bitmaps
		if (mTileWidth != newWidth) {
			mTileWidth = newWidth;
			mImageSize = new ImageSize(newWidth, newWidth);

			mEmptyBitmap = drawableToScaledBitmap(R.drawable.question_mark_red);
			mErrorBitmap = drawableToScaledBitmap(R.drawable.failed_red);
			mCancelBitmap = drawableToScaledBitmap(R.drawable.cancel_image);
			mLoadingBitmap = drawableToScaledBitmap(R.drawable.loader);
		}

		int minh = (mTileWidth + mTilePadding) * mRows + mTilePadding + getPaddingBottom() + getPaddingTop();
		int h = ViewCompat.resolveSizeAndState(minh, heightMeasureSpec, 0);

		setMeasuredDimension(w, h);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		// just for edit mode in Eclipse layout viewer
		if (isInEditMode()) {
			mTileWidth = (int) ((canvas.getWidth() - mTilePadding) / mColumns - mTilePadding);
		}

		// call internal draw method
		internalDraw(canvas);
	}

	/* =========================== HANDY INTERNAL METHODS =========================== */

	/**
	 * Iterate over images and draw them on view canvas.
	 *
	 * @param canvas
	 *            the canvas
	 */
	private void internalDraw(Canvas canvas) {
		Iterator<CroppedImage> imageIterator = mImages.iterator();
		for (int y = 0; y < mRows; y++) {
			for (int x = 0; x < mColumns; x++) {
				CroppedImage next = null;

				if (imageIterator.hasNext())
					next = imageIterator.next();

				int left = (x + 1) * mTilePadding + x * mTileWidth;
				int top = (y + 1) * mTilePadding + y * mTileWidth;

				// if there's no more images then draw empty bitmap
				if (next != null) {
					next.draw(canvas, left, top, mImageSize, mPaint);
				} else {
					canvas.drawBitmap(mEmptyBitmap, left, top, mPaint);
				}
			}
		}
	}

	/**
	 * Convert 9-patch {@link Drawable} to scaled {@link Bitmap}.
	 *
	 * @param drawable
	 *            the drawable
	 * @return the bitmap
	 */
	private Bitmap drawableToScaledBitmap(@DrawableRes int drawable) {
		InputStream is = getResources().openRawResource(drawable);
		Bitmap source = BitmapFactory.decodeStream(is);
		return Bitmap.createScaledBitmap(source, mTileWidth, mTileWidth, false);
	}

	/**
	 * Request layout if necessary.
	 */
	private void requestLayoutIfNecessary() {
		if (getSize() > 0) {
			requestLayout();
			invalidate();
		}
	}

	/* =========================== IMAGE LOADABLE CALLBACKS =========================== */

	@Override
	public Bitmap getErrorImage() {
		return mErrorBitmap;
	}

	@Override
	public Bitmap getCanceledImage() {
		return mCancelBitmap;
	}

	@Override
	public Bitmap getLoadingImage() {
		return mLoadingBitmap;
	}
}
