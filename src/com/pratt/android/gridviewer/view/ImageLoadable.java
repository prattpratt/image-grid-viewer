package com.pratt.android.gridviewer.view;

import android.graphics.Bitmap;

/**
 * Define interface for classes that process image loading and displaying events
 */
public interface ImageLoadable {

	/**
	 * Request re-draw event
	 */
	public void invalidate();

	/**
	 * Gets the image for error loading events
	 */
	public Bitmap getErrorImage();

	/**
	 * Gets the image for canceled loading events
	 */
	public Bitmap getCanceledImage();

	/**
	 * Gets the image for loading in progress events
	 */
	public Bitmap getLoadingImage();
}
