package com.pratt.android.gridviewer.activity;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.ListView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.listener.PauseOnScrollListener;
import com.pratt.android.gridviewer.R;
import com.pratt.android.gridviewer.adapters.ImageGridsListAdapter;
import com.pratt.android.gridviewer.providers.ImageRetriver;

public class MainActivity extends ActionBarActivity {

	/** The list view. */
	private ListView mListView;

	/** The adapter for list of image grids */
	private ImageGridsListAdapter mListAdapter;

	/** The retriver of images from MediaStore */
	private ImageRetriver mRetriver;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mListView = (ListView) findViewById(R.id.imageGridsList);

		// Create default options which will be used for every
		// displayImage(...) call if no options will be passed to this method
		DisplayImageOptions displayOptions = new DisplayImageOptions.Builder()
				.cacheInMemory(true)
				.cacheOnDisk(false)
				.build();

		// Create global configuration and initialize ImageLoader with this config
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
				.defaultDisplayImageOptions(displayOptions)
				.memoryCacheSizePercentage(50)
				.writeDebugLogs()
				.build();
		ImageLoader.getInstance().init(config);

		mRetriver = new ImageRetriver(getContentResolver());
		mRetriver.retrive();

		mListAdapter = new ImageGridsListAdapter(this, mRetriver.getImages());
		mListView.setAdapter(mListAdapter);
		mListView.setOnScrollListener(new PauseOnScrollListener(ImageLoader.getInstance(), false, true));
	}

}
