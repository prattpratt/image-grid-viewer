package com.pratt.android.gridviewer.adapters;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.pratt.android.gridviewer.R;
import com.pratt.android.gridviewer.providers.ImageGridItem;
import com.pratt.android.gridviewer.view.ImageGridView;

public class ImageGridsListAdapter extends ArrayAdapter<ImageGridItem> {

	/**
	 * The View Holder class
	 */
	static class ViewHolder {
		TextView dateTitle;
		ImageGridView imageGrid;
	}

	public ImageGridsListAdapter(Context context, ImageGridItem[] items) {
		super(context, 0, items);
	}

	public ImageGridsListAdapter(Context context, List<ImageGridItem> items) {
		super(context, 0, items);
	}

	public ImageGridsListAdapter(Context context) {
		super(context, 0);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		ViewHolder holder;

		// apply traditional View Holder pattern
		if (view == null) {
			LayoutInflater inflater = LayoutInflater.from(getContext());
			view = inflater.inflate(R.layout.list_view_row, parent, false);
			holder = new ViewHolder();

			holder.dateTitle = (TextView) view.findViewById(R.id.dateTitle);
			holder.imageGrid = (ImageGridView) view.findViewById(R.id.imageGridView);

			view.setTag(holder);
		} else {
			holder = (ViewHolder) view.getTag();
		}

		// extracting targeting object for current view from array
		ImageGridItem item = getItem(position);
		String dateTaken = item.getDateTaken();
		List<String> imageURIs = item.getImageURIs();
		holder.dateTitle.setText(dateTaken);
		// calculate rows number
		int imageNumber = imageURIs.size();
		int columnsNumber = holder.imageGrid.getColumns();
		int rowsNumber = (int) Math.ceil((double) imageNumber / columnsNumber);
		// and image URIs
		holder.imageGrid.setImages(imageURIs);
		// setting rows number
		holder.imageGrid.setRows(rowsNumber);

		return view;
	}
}
